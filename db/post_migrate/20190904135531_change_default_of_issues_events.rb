class ChangeDefaultOfIssuesEvents < ActiveRecord::Migration[5.2]
  include Gitlab::Database::MigrationHelpers

  DOWNTIME = false

  def up
    change_column_default :web_hooks, :issues_events, false
  end

  def down
    change_column_default :web_hooks, :issues_events, nil
  end
end
